package Lista_4;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Random;


class WinnerWasCalled extends Exception {
}
class Too_many_turns extends Exception{
	
}
class number_of_players extends Exception
{
	
}


class Log {

	public static void info() {
		System.out.println("");
	}

	public static void info(String message) {
		System.out.println(message);
	}
}

class Cube
{
	private static int number_wall;

public Cube(int number_wall) {
	 this.number_wall=number_wall ;
	}
public static int get_wall()
{
	return number_wall;
}
}
class max_position
{
	private static int max_position;

public max_position(int max_position) {
	 this.max_position=max_position ;
	}
public static int get_max_position()
{
	return max_position;
}
}


class Dice {

	public int roll() {
		Random rand = new Random();
		int result = rand.nextInt(Cube.get_wall()) + 1;

		Log.info("Dice roll: " + result);
		return result;
	}

}

class Pawn {

	private int position;
	private String name;

	public Pawn(String name) {
		this.name=name;
		Log.info(this.name + " joined the game.");
	}
	
	public void set_Pawn_position(int position) {
		this.position =this.position+position;
		
	}
	
	public String get_Pawn_name() {
	    return name;
	  }
	
	public int get_Pawn_position() {
	    return position;
	  }

}

class number_Player {
	
	public static int count;
	public number_Player(int count)  throws number_of_players {
		this.count =count;
		 if(3>number_Player.count||number_Player.count>10) {
			throw new number_of_players();
		}
		
	}
}

class Board {

	public ArrayList<Pawn> pawns;
	public Dice dice;
	public Pawn winner;
	public static int turnsCounter;
	public static int max_turns;

	public Board() {
		this.pawns = new ArrayList<Pawn>();
		this.dice = null;
		this.winner = null;
		this.turnsCounter = 0;
		this.max_turns=0;
	}
	public void performTurn() throws WinnerWasCalled, Too_many_turns {
		this.turnsCounter++;
		Log.info();
		Log.info("Turn " + this.turnsCounter);

		for(Pawn pawn : this.pawns) {
			int rollResult = this.dice.roll();
			pawn.set_Pawn_position(rollResult) ;
			Log.info(pawn.get_Pawn_name() + " new position: " + pawn.get_Pawn_position());

			if(pawn.get_Pawn_position() >= max_position.get_max_position()) {
				this.winner = pawn;
				throw new WinnerWasCalled();
			}
				else if(Board.turnsCounter >= Board.max_turns) {
					throw new Too_many_turns();
				}
				
			}
			
	
}
	public void Too_many_turns() throws Too_many_turns {
		
	}
}
	

public class Main {

	public static void main(String[] args) throws Too_many_turns {
		Scanner sc = new Scanner(System.in);
		
		Log.info("Podaj liczb� �cian kostki");
		Cube cube=new Cube(sc.nextInt());
		Log.info("Podaj liczb� pola maxymalnego");
		max_position max_position=new max_position(sc.nextInt());
		Board board = new Board();
		Log.info("Podaj liczb� tur");
		Board.max_turns=sc.nextInt();
		board.dice = new Dice();
		Log.info("Podaj liczbe graczy");
		try{
			new number_Player(sc.nextInt());
		}catch(number_of_players exception)
		{
			Log.info();
			Log.info("Podano z�� liczb� graczy");
		}
		for(int x=0;x< number_Player.count;x++)
		{
		Log.info("Podaj nazw� gracza");	
		if(x==0)
			{
			String name=sc.nextLine(); //nie wiem czemu ale na pocz�tku pomija pierwsze wpisanie z kalwiatury nie wiedzia�em jak to rozwi�za� 
			}
		board.pawns.add(new Pawn(sc.nextLine()));
		}
		try {
			while(true) {
				board.performTurn();
			}
		} catch(WinnerWasCalled exception) {
			Log.info();
			Log.info(board.winner.get_Pawn_name() + " won.");
		}
		catch(Too_many_turns exception) {
			Log.info();
			Log.info("Przekroczono limit tur");
		}
		
	}


}